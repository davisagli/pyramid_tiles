import colander
import deform
from substanced.schema import Schema
from pyramid_tiles import tile


class TextTileSchema(Schema):

	innerHTML = colander.SchemaNode(
        colander.String(),
        widget=deform.widget.RichTextWidget()
        )

	css_class = colander.SchemaNode(colander.String())


@tile(name='text', schema=TextTileSchema(), renderer='text_tile.pt')
def text_tile(request, data):
	return data
